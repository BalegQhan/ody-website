
var viewport = document.querySelector("meta[name=viewport]");
var headerText = document.getElementById('header-text');

 if (window.screen.width < 700 ) {
    //viewport.setAttribute('content', 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0');
    viewport.setAttribute('content', 'width=device-width, initial-scale=.33');
    headerText.style.paddingLeft = '16%';

 } else {
    viewport.setAttribute('content', 'width=device-width, initial-scale=1.0');
 }
